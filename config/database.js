var sqlite3 = require("sqlite3").verbose();
var md5 = require("md5");

const DBSOURCE = "db.sqlite";

let db = new sqlite3.Database(DBSOURCE, (err) => {
  if (err) {
    // Cannot open database
    console.error(err.message);
    throw err;
  } else {
    console.log("Connected to the SQlite database.");

    /**
     * Populate User table
     */
    db.run(
      `CREATE TABLE user (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name text, 
            email text UNIQUE, 
            password text, 
            CONSTRAINT email_unique UNIQUE (email)
            )`,
      (err) => {
        if (err) {
          // Table already created
        } else {
          // Table just created, creating some rows
          var insert =
            "INSERT INTO user (name, email, password) VALUES (?,?,?)";
          db.run(insert, ["admin", "admin@example.com", md5("admin123456")]);
          db.run(insert, ["user", "user@example.com", md5("user123456")]);
        }
      }
    );

    /**
     * Populate CountryDetail table
     */

    db.run(
      `CREATE TABLE country_detail (
              id INTEGER PRIMARY KEY AUTOINCREMENT,
              country_name text UNIQUE, 
              gmt_offset text,
              CONSTRAINT country_name_unique UNIQUE (country_name)
              )`,
      (err) => {
        if (err) {
          // Table already created
        } else {
          // Table just created, creating some rows
          var insert =
            "INSERT INTO country_detail (country_name, gmt_offset) VALUES (?,?)";
          db.run(insert, ["Singapore", "GMT 8+"]);
          db.run(insert, ["Bangladesh", "GMT 6+"]);
        }
      }
    );
  }
});

module.exports = db;
