const db = require("../config/database");

exports.findAll = (req, res) => {
  var sql = "select * from country_detail";
  var params = [];
  db.all(sql, params, (err, rows) => {
    if (err) {
      res.status(500).json({ error: "Internal Server Error" });
      return;
    }
    res.json({
      message: "success",
      data: rows || [],
    });
  });
};

exports.findByCountryName = (req, res) => {
  var sql = "select * from country_detail where country_name = ?";
  var params = [req.params.countryName];
  db.get(sql, params, (err, row) => {
    if (err) {
      res.status(500).json({ error: "Internal Server Error" });
      return;
    }

    if (!row) {
      res.status(404).json({ error: "Not Found" });
      return;
    }
    res.json({
      message: "success",
      data: row,
    });
  });
};
