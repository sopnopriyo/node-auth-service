const express = require("express");
const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const { httpPort } = require("./config/const");

// Start server
app.listen(httpPort, () => {
  console.log("Server running on port %PORT%".replace("%PORT%", httpPort));
});

/**
 * -----------------------------------------------------------------------------
 * Routers
 */
app.get("/", (req, res, next) => {
  res.json({ message: "Ok" });
});

const authController = require("./controller/authController");
const countryDetailController = require("./controller/countryDetailController");
const authMiddleware = require("./middleware/auth");

app.post("/login", authController.login);
app.get("/logout", authController.logout);
app.post("/token", authController.token);

app.get(
  "/countryDetails",
  authMiddleware.authenticateJWT,
  countryDetailController.findAll
);
app.get(
  "/countryDetails/:countryName",
  authMiddleware.authenticateJWT,
  countryDetailController.findByCountryName
);
