const db = require("../config/database");
const jwt = require("jsonwebtoken");
const md5 = require("md5");

const {
  accessTokenSecret,
  refreshTokenSecret,
  tokenLife,
  refreshTokenLife,
} = require("../config/const");

/**
 * Refresh token is in-memory only for demonstration purpose, it must be replaced in production.
 * We can store the refresh token either in DB or in cache server so that all the other instance can
 * access it
 */
const refreshTokens = [];

exports.login = (req, res) => {
  // read username and password from request body
  const { email, password } = req.body;

  var sql = "select * from user where email = ? AND password = ?";
  var params = [email, md5(password)];
  db.get(sql, params, (err, user) => {
    if (err) {
      res.status(500).json({ error: err });
      return;
    }

    if (!user) {
      res.status(404).json({ error: "Username/Password not found" });
      return;
    }
    // generate an access token
    const accessToken = jwt.sign({ email: user.email }, accessTokenSecret, {
      expiresIn: tokenLife,
    });

    const refreshToken = jwt.sign({ email: user.email }, refreshTokenSecret, {
      expiresIn: refreshTokenLife,
    });

    refreshTokens.push(refreshToken);

    res.json({
      message: "success",
      data: {
        accessToken,
        refreshToken,
      },
    });
  });
};

exports.logout = (req, res) => {
  const { token } = req.body;
  refreshTokens = refreshTokens.filter((token) => t !== token);

  res.send("Logout successful");
};

exports.token = (req, res) => {
  const { token } = req.body;

  if (!token) {
    return res.sendStatus(401);
  }

  if (!refreshTokens.includes(token)) {
    return res.sendStatus(403);
  }

  jwt.verify(token, refreshTokenSecret, (err, user) => {
    if (err) {
      return res.sendStatus(403);
    }

    const accessToken = jwt.sign({ email: user.email }, accessTokenSecret, {
      expiresIn: tokenLife,
    });

    res.json({
      accessToken,
    });
  });
};
