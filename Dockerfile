FROM node:10-alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . .

RUN npm install

RUN npm rebuild

EXPOSE 3000

CMD ["npm", "run", "start"]