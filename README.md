# Node JS Web service

## For local testing

1. Install the packages
   `npm install`

2. Run the project
   `npm run start`

## Docker build and run

1. Build the Image
   `docker build -t sopnopriyo/node-auth-service .`
2. Run the docker container
   `docker run -p3000:3000 sopnopriyo/node-auth-service`

## Future Enhancements

    1. Use ORM for accessing DB rather than writing SQL query
    2. Use DB revision package
    3. Use design pattern
    4. Use caching to reduce the number of queries into DB
    5. Write Unit Test and Integration Test
    6. Jenkinsfile for CI/CD
    7. Sonarqube static code analysis
    8. Configuration for dev and prod environment
    9. Swagger documentation
    10. Logging
    11. Distributed refresh token
